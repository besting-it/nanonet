# NanoNet

NanoNet is a very simple Java neural network library using backpropagation and sigmoid activation function.

Licensed under the Apache License, Version 2.0.

# Quick start
Here's a classic example for XOR.

The network definition.

```java
// Two inputs, one hidden layer, one output
System.out.println("Testing XOR"); 
Network network = new Network();
network.addLayer(new Layer().addNeurons(2), INPUT)
		.addLayer(new Layer().addNeurons(2), HIDDEN)
		.addLayer(new Layer().addNeurons(1), OUTPUT); 
```

The training.
```java
// Definition of input and expected values
double [] in1 = {0, 0};
double [] in2 = {0, 1};
double [] in3 = {1, 0};
double [] in4 = {1, 1};
double [] ex1 = {0};
double [] ex2 = {1};
double [] ex3 = {1};
double [] ex4 = {0};
Object [] allIn = {in1, in2, in3, in4};
Object [] allEx = {ex1, ex2, ex3, ex4};
// Training, 5000 steps, learning rate from 2.0 to 0.1
int steps = 5000;
network.setAutoLearningRate(2.0, 0.1, steps);
Random rnd = new Random(0);
for (int i = 0; i < steps; i++) {
	int next = rnd.nextInt(4);
	network.train((double[]) allIn[next], (double[]) allEx[next]);
}
```

The evaluation.
```java
network.evaluate(new double [] {0, 1});
double output = network.getOutputs()[0];
System.out.println(output);
```
