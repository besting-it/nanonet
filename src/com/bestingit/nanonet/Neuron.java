/**
 * Copyright 2019 Andreas Besting, info@besting-it.de
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.bestingit.nanonet;

import java.util.LinkedList;

public class Neuron {
    
    private double value;
    private boolean bias;
    private final LinkedList<Synapse> incConnections = new LinkedList<>();
    private final LinkedList<Synapse> outConnections = new LinkedList<>();
    // Partial derivative values
    protected double d_Out_d_Net, d_E_d_Out;
    
    public Neuron() {
        this(0, false);
    }
    
    public Neuron(double value) {
        this(value, false);
    }
    
    public Neuron(double value, boolean bias) {
        this.value = value;
        this.bias = bias;
    }
    
    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void setBias(boolean bias) {
        this.bias = bias;
    }

    public boolean isBias() {
        return bias;
    }

    public LinkedList<Synapse> getIncConnections() {
        return incConnections;
    }

    public LinkedList<Synapse> getOutConnections() {
        return outConnections;
    }

    public boolean isInputNeuron() {
        return incConnections.isEmpty();
    }
    
    public boolean isOutputNeuron() {
        return outConnections.isEmpty();
    }   

}
