/**
 * Copyright 2019 Andreas Besting, info@besting-it.de
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.bestingit.nanonet;

import com.bestingit.nanonet.Layer.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Network {
    
    interface IActivationFunction {

        double getActivation(double x);

    }    
    
    class SigmoidFunction implements IActivationFunction {

        @Override
        public double getActivation(double x) {
            return 1.0 / (1.0 + Math.exp(-x));
        }

    }    
    
    private final List<Layer> layers = new ArrayList<>();
    
    private IActivationFunction actFuntion;
    private double learningValue = 0.5;
	private double learningStepSize = 0;
    private final Random rnd = new Random(0);

    private void setActFuntion(IActivationFunction actFuntion) {
        this.actFuntion = actFuntion;
    }

    private IActivationFunction getActFuntion() {
        if (actFuntion == null)
            setActFuntion(new SigmoidFunction());        
        return actFuntion;
    }

    /**
     * Get learning rate.
     * @return The learning rate
     */
    public double getLearningValue() {
        return learningValue;
    }

    /**
     * Set learning rate.
     * @param learningValue The learning rate 
     */
    public void setLearningRate(double learningValue) {
        this.learningValue = learningValue;
		learningStepSize = 0;
    }
	
    /**
     * Initialize automatic learning rate adjustment. Each training call will adjust the current rate.
     * @param startRate The start rate
	 * @param endRate The end rate
	 * @param steps The number of training steps to perform
     */
    public void setAutoLearningRate(double startRate, double endRate, int steps) {
        setLearningRate(startRate);
		learningStepSize = (startRate - endRate) / steps;
    }	
    
    /**
     * Add layer to network. Automatically assigns bias nodes.
     * @param layer The layer
     * @param isOutput Whether layer is output layer (last layer)
     * @return The network
     */
    public Network addLayer(Layer layer, Type type) {
        // Add bias to layer
        if (type != Type.OUTPUT) 
            layer.addNeuron(new Neuron(1, true));
        layers.add(layer);
        int prev = layers.size() - 2;
        if (prev < 0)
            return this;
        final Layer prevLayer = layers.get(prev);
        // Create connections between layers
        prevLayer.getNeurons().stream().forEach((from) -> {
            layer.getNeurons().stream().forEach((to) -> {
                final Synapse synapseTo = new Synapse(to, rnd.nextDouble() + Double.MIN_VALUE);
                from.getOutConnections().add(synapseTo);
                if (!to.isBias()) {
                    final Synapse synapseFrom = new Synapse(from, rnd.nextDouble() + Double.MIN_VALUE);
                    to.getIncConnections().add(synapseFrom);
                    // Link weights (adjusting "from" adjusts "to" for bidirectional connection)
                    synapseTo.setWeightRef(synapseFrom.getWeightRef());                    
                }
            });
        });
        return this;
    }

    private void update(Neuron n) {
        if (n.isBias())
            return;
        if (n.getIncConnections().isEmpty())
            return;
        double sum = 0;
        for (Synapse s : n.getIncConnections()) {
            sum += s.getNeuron().getValue() * s.getWeight();
        }
        n.setValue(getActFuntion().getActivation(sum));
    }  

    /**
     * Get layers
     * @return The layers
     */
    public List<Layer> getLayers() {
        return layers;
    }
    
    /**
     * Feed forward.
     * @param inputs The inputs 
     */
    public void evaluate(double [] inputs) {
        if (inputs.length != layers.get(0).getNeurons().size() - 1)
            throw new ArrayIndexOutOfBoundsException("Input length out of bounds!");
        // Set input values
        Layer input = layers.get(0);
        for (int i = 0; i < inputs.length; i++) {
            input.getNeurons().get(i).setValue(inputs[i]);
        }
        for (int l = 1; l < layers.size(); l++) {
            Layer next = layers.get(l);
            next.getNeurons().stream().forEach((n) -> {
                update(n);
            });
        }
    }
    
    /**
     * Get outputs.
     * @return The outputs
     */
    public double [] getOutputs() {
        return layers.get(layers.size() - 1).getOutputs();
    }
    
    /**
     * Train.
     * @param inputs The inputs
     * @param expected The expected values
     */
    public void train(double [] inputs, double [] expected) {
        evaluate(inputs);
        backPropOutputLayer(expected);
        backPropHiddenLayers();
		if (learningStepSize > 0) {
			learningValue -= learningStepSize;
		}
    }
    
    private void backPropOutputLayer(double[] expected) {
        Layer output = layers.get(layers.size() - 1);
        double [] outputs = output.getOutputs();
        List<Neuron> outNeurons = output.getNeurons();
        for (int i = 0; i < outputs.length; i++) {
            final Neuron neuron = outNeurons.get(i);
            neuron.d_E_d_Out = outputs[i] - expected[i];
            neuron.d_Out_d_Net = outputs[i] - outputs[i] * outputs[i];
            updateWeights(neuron);
        }
    }    

    private void backPropHiddenLayers() {
        for (int l = layers.size() - 2; l > 0; l--) {
            Layer hidden = layers.get(l);
            List<Neuron> neurons = hidden.getNeurons();
            for (Neuron next : neurons) {
                // Sum up hidden to next
                double d_ETotal_d_Out = 0;
                for (Synapse s : next.getOutConnections()) {
                    double d_E_d_Net = s.getNeuron().d_E_d_Out * s.getNeuron().d_Out_d_Net;
                    double d_Net_d_Out = s.getWeight();
                    double d_E_d_Out = d_E_d_Net * d_Net_d_Out;
                    d_ETotal_d_Out += d_E_d_Out;
                }
                // Update neuron
                next.d_E_d_Out = d_ETotal_d_Out;
                next.d_Out_d_Net = next.getValue() - next.getValue() * next.getValue();
                // Prev to hidden
                updateWeights(next);              
            }
        }       
    }

    private void updateWeights(final Neuron neuron) {
        neuron.getIncConnections().stream().forEach((s) -> {
            double d_Net_d_W = s.getNeuron().getValue();
            double d_E_d_W = neuron.d_E_d_Out * neuron.d_Out_d_Net * d_Net_d_W;
            s.adjust(learningValue * d_E_d_W);
        });
    }

   
}
