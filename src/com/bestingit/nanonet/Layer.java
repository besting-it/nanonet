/**
 * Copyright 2019 Andreas Besting, info@besting-it.de
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.bestingit.nanonet;

import java.util.ArrayList;
import java.util.List;

public class Layer {
    
	public enum Type {
		INPUT,
		HIDDEN,
		OUTPUT
	}
	
    private final List<Neuron> neurons = new ArrayList<>();

    public List<Neuron> getNeurons() {
        return neurons;
    }
    
    public Layer addNeuron(Neuron neuron) {
        neurons.add(neuron);
        return this;
    }
    
    public Layer addNeurons(int count) {
        for (int i = 0; i < count; i++) {
            neurons.add(new Neuron());
        }
        return this;
    }    
    
    public double [] getOutputs() {
        double [] d = new double[neurons.size()];
        for (int i = 0; i < d.length; i++) {
            d[i] = neurons.get(i).getValue();
        }
        return d;
    }    
   
}
