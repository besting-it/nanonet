package com.bestingit.nanonet.test;

import com.bestingit.nanonet.Layer;
import static com.bestingit.nanonet.Layer.Type.*;
import com.bestingit.nanonet.Network;
import java.text.DecimalFormat;
import java.util.Random;
import static org.junit.Assert.*;
import org.junit.Test;

public class NetworkTest {
    
    public NetworkTest() {
    }
    
    @Test
    public void xor() {
        // Two inputs, one hidden layer, one output
        System.out.println("Testing XOR"); 
        Network network = new Network();
        network.addLayer(new Layer().addNeurons(2), INPUT)
                .addLayer(new Layer().addNeurons(2), HIDDEN)
                .addLayer(new Layer().addNeurons(1), OUTPUT);            
        double [] in1 = {0, 0};
        double [] in2 = {0, 1};
        double [] in3 = {1, 0};
        double [] in4 = {1, 1};
        double [] ex1 = {0};
        double [] ex2 = {1};
        double [] ex3 = {1};
        double [] ex4 = {0};
        Object [] allIn = {in1, in2, in3, in4};
        Object [] allEx = {ex1, ex2, ex3, ex4};
        int steps = 5000;
		network.setAutoLearningRate(2.0, 0.1, steps);
        Random rnd = new Random(0);
        for (int i = 0; i < steps; i++) {
            int next = rnd.nextInt(4);
            network.train((double[]) allIn[next], (double[]) allEx[next]);
            outputCurrentEvaluation(network, in1, in2, in3, in4);
        }  
        double [] outs;
        network.evaluate(in1);
        outs = network.getOutputs();
        assertEquals(outs[0], 0, 0.1);
        network.evaluate(in2);
        outs = network.getOutputs();        
        assertEquals(outs[0], 1, 0.1);
        network.evaluate(in3);
        outs = network.getOutputs();        
        assertEquals(outs[0], 1, 0.1);
        network.evaluate(in4);
        outs = network.getOutputs();        
        assertEquals(outs[0], 0, 0.1);        
    }

	private void outputCurrentEvaluation(Network network, double[] in1, double[] in2, double[] in3, double[] in4) {
        DecimalFormat df = new DecimalFormat("0.000000");
		network.evaluate(in1);
		double [] out1 = network.getOutputs();
		network.evaluate(in2);
		double [] out2 = network.getOutputs();
		network.evaluate(in3);
		double [] out3 = network.getOutputs();
		network.evaluate(in4);
		double [] out4 = network.getOutputs();
		System.out.println("Network output: "
				+ df.format(out1[0]) + "; "
				+ df.format(out2[0]) + "; "
				+ df.format(out3[0]) + "; "
				+ df.format(out4[0]));
	}
    
}
